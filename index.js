const btnWrapper = document.querySelectorAll(".btn-wrapper .btn");

function changeButtonColor(button) {
    btnWrapper.forEach((btn) => {
        btn.style.backgroundColor = "";
    });
    button.style.backgroundColor = "blue";
}

document.addEventListener("keydown", function (event) {
    event.preventDefault();
    const key = event.key.toUpperCase();
    const button = findButtonByKey(key);
    if (button) {
        changeButtonColor(button);
    } else if (key === "ENTER") {
        const enterButton = document.querySelector('.btn-wrapper [data-key="Enter"]');
        if (enterButton) {
            changeButtonColor(enterButton);
        }
    }
});

function findButtonByKey(key) {
    return Array.from(btnWrapper).find((button) => button.dataset.key === key);
}




